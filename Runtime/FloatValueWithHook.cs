﻿using System;
using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
  [CreateAssetMenu(menuName = "Values/FloatWithHook")]
  public class FloatValueWithHook : FloatValue
  {
    public Action RunTimeValueChanged;

    public override float RunTimeValue {
      get => base.RunTimeValue;
      set 
      {
        base.RunTimeValue = value;
        RunTimeValueChanged?.Invoke();
      } 
    }
  }
}