﻿using System;
using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
  [CreateAssetMenu(menuName = "Values/Float")]
  public class FloatValue : ScriptableObject
  {
    [SerializeField] protected float _initialValue;
    public virtual float RunTimeValue { get => runTimeValue; set => runTimeValue = value; }
    [SerializeField] private float runTimeValue;

    public event EventHandler InitialValueChangedEvent;
    
    public float GetInitialValue()
    {
      return _initialValue;
    }
    
    public void SetInitialValue(float value)
    {
      _initialValue = value;
      InitialValueChangedEvent?.Invoke(this, EventArgs.Empty);
    }

    private void OnEnable()
    {
      Reset();
    }

    public void Reset()
    {
      RunTimeValue = GetInitialValue();
    }
  }
}
