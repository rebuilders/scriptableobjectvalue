﻿using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
  [CreateAssetMenu(menuName = "Values/Boolean")]
  public class BoolValue : ScriptableObject
  {
    [SerializeField] private bool _initialValue;

    [SerializeField] private bool _runTimeValue;

    public bool RunTimeValue {
      get
        => _runTimeValue;
      set
        => _runTimeValue = value;
    }

    public bool GetInitialValue()
    {
      return _initialValue;
    }
    public void SetInitialValue(bool value)
    {
      _initialValue = value;
    }

    private void OnEnable()
    {
      _runTimeValue = GetInitialValue();
    }
  }
}
