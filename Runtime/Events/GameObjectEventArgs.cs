﻿using UnityEngine;
using UnityEngine.Analytics;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events
{
  public class GameObjectEventArgs : System.EventArgs
  {
    public GameObject Go;
  }
}
