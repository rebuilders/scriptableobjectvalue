﻿using System.Collections.Generic;
using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events
{
  [CreateAssetMenu(menuName = "Events/GameEvent")]
  public class GameEvent : ScriptableObject
  {
    private readonly List<IGameEventListener> _listeners = new List<IGameEventListener>();

    public virtual void Raise()
    {
      Debug.Log($"Raised event: {name}");
      for (var i = _listeners.Count - 1; i >= 0; i--)
      {
        _listeners[i].OnEventRaised();
      }
    }

    public void RegisterListener(IGameEventListener gameEventListener)
    {
      _listeners.Add(gameEventListener);
    }

    public void UnregisterListener(IGameEventListener gameEventListener)
    {
      _listeners.Remove(gameEventListener);
    }
  }

  public abstract class GameEvent<T> : ScriptableObject
  {
    private readonly List<IGameEventListener<T>> _listeners = new List<IGameEventListener<T>>();

    public void Raise(T value)
    {
      Debug.Log($"Raised event: {name}");
      for (var i = _listeners.Count - 1; i >= 0; i--)
      {
        _listeners[i].OnEventRaised(value);
      }
    }

    public void RegisterListener(IGameEventListener<T> gameEventListener)
    {
      _listeners.Add(gameEventListener);
    }

    public void UnregisterListener(IGameEventListener<T> gameEventListener)
    {
      _listeners.Remove(gameEventListener);
    }
  }
}