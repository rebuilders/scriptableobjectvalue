﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events
{
  public class MultiGameEventListener : MonoBehaviour
  {
    private readonly List<SingleGameEventListener> _listeners = new List<SingleGameEventListener>();
    private ConcurrentDictionary<GameEvent, byte> _awaitedEvents;
    public List<GameEvent> Events;
    public UnityEvent Response;

    public virtual void ResetListener()
    {
      DeactivateListeners();
      ActivateListeners();
    }

    private void OnEnable()
    {
      ActivateListeners();
    }

    private void OnDisable()
    {
      DeactivateListeners();
    }

    private ConcurrentDictionary<GameEvent, byte> ResetAwaitedEvents()
    {
      var awaitedEvents = new ConcurrentDictionary<GameEvent, byte>();
      Events.RemoveAll(e => e == null);
      Events.ForEach(e => awaitedEvents[e] = Byte.MinValue);
      return awaitedEvents;
    }

    private void ActivateListeners()
    {
      _awaitedEvents = ResetAwaitedEvents();
      foreach (var gameEvent in Events)
      {
        if (!gameEvent)
        {
          continue;
        }

        var listener = new SingleGameEventListener(gameEvent, this);
        this._listeners.Add(listener);
        gameEvent.RegisterListener(listener);
      }
    }

    private void DeactivateListeners()
    {
      foreach (var listener in _listeners)
      {
        listener.GameEvent.UnregisterListener(listener);
      }

      _listeners.Clear();
    }

    private void OnEventRaised(GameEvent gameEvent)
    {
      _awaitedEvents.TryRemove(gameEvent, out var ignored);
      if (_awaitedEvents.IsEmpty == false)
        return;

      Response.Invoke();
      _awaitedEvents = ResetAwaitedEvents();
    }

    private class SingleGameEventListener : IGameEventListener
    {
      private readonly MultiGameEventListener _parent;
      public readonly GameEvent GameEvent;

      public SingleGameEventListener(GameEvent gameEvent, MultiGameEventListener parent)
      {
        this.GameEvent = gameEvent;
        this._parent = parent;
      }

      public void OnEventRaised()
      {
        _parent.OnEventRaised(GameEvent);
      }
    }
  }
}