using System;
using UnityEngine.Events;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events
{
  [Serializable]
  public class IntUnityEvent : UnityEvent<int>
  {
    
  }
}