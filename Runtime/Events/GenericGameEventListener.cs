using UnityEngine;
using UnityEngine.Events;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events
{
  public abstract class GameEventListener<T> : MonoBehaviour, IGameEventListener<T>
  {
    public GameEvent<T> Event;
    
    private void OnEnable()
    {
      if(Event)
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
      if(Event)
        Event.UnregisterListener(this);
    }

    public abstract void OnEventRaised(T value);
  }
}