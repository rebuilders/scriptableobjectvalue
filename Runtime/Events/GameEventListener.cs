﻿using UnityEngine;
using UnityEngine.Events;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events
{
  public class GameEventListener : MonoBehaviour, IGameEventListener
  {
    public GameEvent Event;
    public UnityEvent Response;

    private void OnEnable()
    {
      if(Event)
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
      if(Event)
        Event.UnregisterListener(this);
    }

    public void OnEventRaised()
    {
      Response.Invoke();
    }
  }
}
