namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events
{
  public interface IGameEventListener
  {
    void OnEventRaised();
  }

  public interface IGameEventListener<T>
  {
    void OnEventRaised(T value);
  }
}