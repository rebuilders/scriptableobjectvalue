using System;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events
{
  [Serializable]
  public class IntGameEventListener : GameEventListener<int>
  {
    public IntUnityEvent Response;
    public override void OnEventRaised(int value)
    {
      Response?.Invoke(value);
    }
  }
}