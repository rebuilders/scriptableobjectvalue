using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events
{
  [CreateAssetMenu(menuName = "Events/IntGameEvent")]
  public class IntGameEvent : GameEvent<int>
  {
    
  }
}