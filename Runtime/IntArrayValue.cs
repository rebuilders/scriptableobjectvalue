﻿using System;
using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
  [CreateAssetMenu(menuName = "Values/IntArray")]
  public class IntArrayValue : ScriptableObject, IReset
  {
    [SerializeField]
    private int[] _initialValue;
    
    public int[] RunTimeValue;

    public Action RunTimeValueChanged;

    public void SetRuntimeValueSilently(int[] value)
    {
      RunTimeValue = value;
    }

    public void SetRunTimeValue(int[] value)
    {
      SetRuntimeValueSilently(value);
      
      RunTimeValueChanged?.Invoke();
    }

    public int[] GetInitialValue()
    {
      return _initialValue;
    }

    public void SetInitialValue(int[] value)
    {
      _initialValue = value;
    }
    
    private void OnEnable()
    {
      SetRunTimeValue(GetInitialValue());
    }

    public void ResetState()
    {
      _initialValue = new int[0];
    }
  }
}
