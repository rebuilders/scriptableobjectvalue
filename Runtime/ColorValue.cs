﻿using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
  [CreateAssetMenu(menuName = "Values/Color")]
  public class ColorValue : ScriptableObject
  {
    [SerializeField] private Color _initialValue;

    public Color RunTimeValue;

    public Color GetInitialValue()
    {
      return _initialValue;
    }
    public void SetInitialValue(Color value)
    {
      _initialValue = value;
    }

    private void OnEnable()
    {
      RunTimeValue = GetInitialValue();
    }
  }
}
