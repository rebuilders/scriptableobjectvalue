﻿using System;
using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
  [CreateAssetMenu(menuName = "Values/String")]
  public class StringValue : ScriptableObject, IReset
  {
    public event Action<string> OnRuntimeValueChanged;
    [SerializeField] private string _initialValue;
    [SerializeField] private string _runTimeValue;
    public string RunTimeValue
    {
      get => _runTimeValue;
      set
      {
        _runTimeValue = value;
        OnRuntimeValueChanged?.Invoke(_runTimeValue);
      }
    }

    public string GetInitialValue()
    {
      return _initialValue;
    }
    
    public void SetInitialValue(string value)
    {
      _initialValue = value;
    }

    private void OnEnable()
    {
      RunTimeValue = GetInitialValue();
    }

    public void ResetState()
    {
      _initialValue = string.Empty;
      RunTimeValue = string.Empty;
    }
  }
}
