﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
  [CreateAssetMenu(menuName = "Values/StringList")]
  public class ListString : ScriptableObject, IReset
  {
    [SerializeField]
    private List<string> _strings = new List<string>();

    public ReadOnlyCollection<string> GetValues()
    {
      return _strings.AsReadOnly();
    }

    public void AddValue(string value)
    {
      _strings.Add(value);
    }
    
    public void ResetState()
    {
      _strings.Clear();
    }

    public void AddValues(IEnumerable<string> itemNames)
    {
      _strings.AddRange(itemNames);
    }
  }
}