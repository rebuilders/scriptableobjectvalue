﻿using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
  [CreateAssetMenu(menuName = "Values/VectorThree")]
  public class VectorThreeValue : ScriptableObject
  {
    [SerializeField] private Vector3 _initialValue;

    public Vector3 RunTimeValue;

    public Vector3 GetInitialValue()
    {
      return _initialValue;
    }
    
    public void SetInitialValue(Vector3 value)
    {
      _initialValue = value;
    }

    private void OnEnable()
    {
      RunTimeValue = GetInitialValue();
    }
  }
}
