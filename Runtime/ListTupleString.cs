﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
  [CreateAssetMenu(menuName = "Values/Tuple<string,string> List")]
  public class ListTupleString : ScriptableObject, IReset
  {
    [SerializeField]
    private List<Tuple<string, string>> _strings = new List<Tuple<string, string>>();

    public System.Collections.ObjectModel.ReadOnlyCollection<Tuple<string, string>> GetValues()
    {
      return _strings.AsReadOnly();
    }

    public void AddValue(string tupleA,string tupleB)
    {
      _strings.Add(new Tuple<string, string>(tupleA,tupleB));
    }
    
    public void AddValues(IEnumerable<Tuple<string,string>> itemNames)
    {
      _strings.AddRange(itemNames);
    }
    
    public void ResetState()
    {
      _strings.Clear();
    }
  }
}