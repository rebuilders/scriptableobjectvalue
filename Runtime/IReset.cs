﻿namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime
{
    public interface IReset
    {
        void ResetState();
    }
}