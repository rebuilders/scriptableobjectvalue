﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events;


namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Editor.Eevents
{
  [CustomEditor(typeof(GameEvent))]
  public class GameEventEditor : UnityEditor.Editor
  {
    List<string> listResult;
    Vector2 scroll;

    private void OnEnable()
    {
      listResult = new List<string>();
      scroll = new Vector2();
    }

    public override void OnInspectorGUI()
    {
      base.OnInspectorGUI();
      GameEvent _GE = target as GameEvent;


      string _guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(_GE));

      EditorGUILayout.TextField("GUID: ", _guid);

      //GUI.enabled = Application.isPlaying;

      if (GUILayout.Button("Raise in Playmode"))
      {
        if (Application.isPlaying)
          _GE.Raise();
        else
          Debug.LogError("Can only raise event in playmode");
      }


      if (listResult.Count == 0)
        GUILayout.Label("No items reference or depend on event " + target.name + " " + listResult.Count);
      else
      {
        GUILayout.Label("The following " + listResult.Count + " items reference or depend on " + target.name + ":");

        scroll = GUILayout.BeginScrollView(scroll);
        foreach (string s in listResult)
        {
          GUILayout.BeginHorizontal();
          GUILayout.Label(s, GUILayout.Width(400));
          if (GUILayout.Button("Select", GUILayout.Width(80)))
          {
            Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(s);
          }
          GUILayout.EndHorizontal();
        }
        GUILayout.EndScrollView();
      }
    }
  }
}
