﻿using AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime;
using UnityEditor;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Editor
{
  [CustomEditor(typeof(ColorValue))]
  public class ColorValueEditor : UnityEditor.Editor
  {
    public override void OnInspectorGUI()
    {
      ColorValue _value = target as ColorValue;
      
      string _guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(_value));
      
      EditorGUILayout.TextField("GUID: ", _guid);
      
      
      base.OnInspectorGUI();
      

      this.Repaint();
    }
  }
}
