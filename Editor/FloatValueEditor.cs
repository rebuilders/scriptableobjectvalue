﻿using AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime;
using UnityEditor;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Editor
{
  [CustomEditor(typeof(FloatValue))]
  public class FloatValueEditor : UnityEditor.Editor
  {
    public override void OnInspectorGUI()
    {
      FloatValue _value = target as FloatValue;
      
      string _guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(_value));
      
      EditorGUILayout.TextField("GUID: ", _guid);
      
      
      base.OnInspectorGUI();
      

      this.Repaint();
    }
  }
}
