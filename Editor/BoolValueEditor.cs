﻿using AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime;
using UnityEditor;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Editor
{
  [CustomEditor(typeof(BoolValue))]
  public class BoolValueEditor : UnityEditor.Editor
  {
    public override void OnInspectorGUI()
    {
      BoolValue _value = target as BoolValue;
      
      string _guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(_value));
      
      EditorGUILayout.TextField("GUID: ", _guid);
      
      
      base.OnInspectorGUI();
      

      this.Repaint();
    }
  }
}
