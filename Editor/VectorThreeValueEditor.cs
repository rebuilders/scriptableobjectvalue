﻿using AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime;
using UnityEditor;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Editor
{
  [CustomEditor(typeof(VectorThreeValue))]
  public class VectorThreeValueEditor : UnityEditor.Editor
  {
    public override void OnInspectorGUI()
    {
      VectorThreeValue _value = target as VectorThreeValue;
      
      string _guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(_value));
      
      EditorGUILayout.TextField("GUID: ", _guid);
      
      
      base.OnInspectorGUI();
      

      this.Repaint();
    }
  }
}
