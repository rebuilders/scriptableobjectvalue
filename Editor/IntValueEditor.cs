﻿using AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime;
using UnityEditor;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Editor
{
  [CustomEditor(typeof(IntValue))]
  public class IntValueEditor : UnityEditor.Editor
  {
    public override void OnInspectorGUI()
    {
      IntValue _value = target as IntValue;
      
      string _guid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(_value));
      
      EditorGUILayout.TextField("GUID: ", _guid);
      
      
      base.OnInspectorGUI();
      

      this.Repaint();
    }
  }
}
