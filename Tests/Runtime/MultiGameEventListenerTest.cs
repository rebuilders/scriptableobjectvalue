using System.Collections;
using System.Collections.Generic;
using AntiEntropy.com.antientropy.scriptableobjectvalues.Runtime.Events;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.TestTools;

namespace AntiEntropy.com.antientropy.scriptableobjectvalues.Tests.Runtime
{
  public class MultiGameEventListenerTest
  {
    private GameEvent _eventA;
    private GameEvent _eventB;
    private GameEvent _eventC;
    private MultiGameEventListener _multiGameEventListener;
    private bool _wasTriggered;

    [SetUp]
    public void SetUp()
    {
      _eventA = ScriptableObject.CreateInstance<GameEvent>();
      _eventB = ScriptableObject.CreateInstance<GameEvent>();
      _eventC = ScriptableObject.CreateInstance<GameEvent>();
      var gameObject = new GameObject();
      gameObject.SetActive(false);
      _multiGameEventListener = gameObject.AddComponent<MultiGameEventListener>();
      _multiGameEventListener.runInEditMode = true;
      _multiGameEventListener.Events = new List<GameEvent> {_eventA, _eventB, _eventC};
      _multiGameEventListener.Response = new UnityEvent();
      _multiGameEventListener.Response.AddListener(() => _wasTriggered = true);
      _wasTriggered = false;

      gameObject.SetActive(true);
    }

    [UnityTest]
    public IEnumerator DontTriggerListener()
    {
      _eventA.Raise();

      yield return null;

      Assert.That(_wasTriggered, Is.False);
    }

    [UnityTest]
    public IEnumerator TriggerListener()
    {
      _eventA.Raise();
      _eventB.Raise();
      _eventC.Raise();

      yield return null;

      Assert.That(_wasTriggered, Is.True);
    }

    [UnityTest]
    public IEnumerator OrderOfEventsIsNotRelevant()
    {
      _eventA.Raise();
      _eventC.Raise();
      _eventB.Raise();

      yield return null;

      Assert.That(_wasTriggered, Is.True);
    }

    [UnityTest]
    public IEnumerator IgnoreNullGameEvents()
    {
      _multiGameEventListener.Events.Add(null);
      _multiGameEventListener.ResetListener();

      yield return null;

      _eventA.Raise();
      _eventC.Raise();
      _eventB.Raise();

      yield return null;

      Assert.That(_wasTriggered, Is.True);
    }

    [UnityTest]
    public IEnumerator TriggerAgainAfterReset()
    {
      _eventA.Raise();
      _eventB.Raise();
      _eventC.Raise();

      yield return null;

      Assert.That(_wasTriggered, Is.True);

      _multiGameEventListener.ResetListener();
      _wasTriggered = false;

      yield return null;

      _eventA.Raise();
      _eventB.Raise();
      _eventC.Raise();

      yield return null;

      Assert.That(_wasTriggered, Is.True);
    }

    [UnityTest]
    public IEnumerator DontTriggerWhenDisabled()
    {
      _multiGameEventListener.gameObject.SetActive(false);
      _eventA.Raise();
      _eventB.Raise();
      _eventC.Raise();

      yield return null;

      Assert.That(_wasTriggered, Is.False);
    }

    [UnityTest]
    public IEnumerator DontTriggerOnMultipleOfTheSame()
    {
      _eventA.Raise();
      _eventA.Raise();
      _eventA.Raise();

      yield return null;

      Assert.That(_wasTriggered, Is.False);
    }
  }
}